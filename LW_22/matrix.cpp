#include <iostream>
#include "matrix.h"

namespace matrix {
	std::istream& operator>>(std::istream& c_input, matrix::Matrix& matrix) {
		for (int i = 0; i < matrix.m_x; i++)
			for (int j = 0; j < matrix.m_y; j++)
				c_input >> matrix.m_matrix[i][j];
		return c_input;
	}

	std::ostream& operator<<(std::ostream& c_output, const matrix::Matrix& matrix) {
		for (int i = 0; i < matrix.m_x; i++) {
			for (int j = 0; j < matrix.m_y; j++)
				c_output << matrix.m_matrix[i][j] << ' ';
			c_output << '\n';
		}
		return c_output;
	}

	Matrix::Matrix(int x, int y) {
		m_x = x;
		m_y = y;
		m_matrix = new int* [m_x];
		for (int i = 0; i < m_x; i++)
			m_matrix[i] = new int [m_y];
	}

	Matrix::Matrix(const Matrix& matrix) {
		m_x = matrix.m_x;
		m_y = matrix.m_y;
		m_matrix = new int* [m_x];
		for (int i = 0; i < m_x; i++)
			m_matrix[i] = new int[m_y];
		for (int i = 0; i < m_x; i++)
			for (int j = 0; j < m_y; j++)
				m_matrix[i][j] = matrix.m_matrix[i][j];
	}

	Matrix& Matrix::operator=(const Matrix& matrix){
		m_x = matrix.m_x;
		m_y = matrix.m_y;
		for (int i = 0; i < m_x; i++)
			for (int j = 0; j < m_y; j++)
				m_matrix[i][j] = matrix.m_matrix[i][j];
		return *this;
	}

	Matrix Matrix::operator+(const Matrix& matrix){
		Matrix tmp(m_x, m_y);
		if ((m_x != matrix.m_x) || (m_y != matrix.m_y)) {
			for (int i = 0; i < m_x; i++)
				for (int j = 0; j < m_y; j++)
					tmp.m_matrix[i][j] = 0;
			std::cout << '\n' << "Operation cancelled!" << '\n';
			return tmp;
		}
		for (int i = 0; i < m_x; i++)
			for (int j = 0; j < m_y; j++)
				tmp.m_matrix[i][j] = m_matrix[i][j] + matrix.m_matrix[i][j];
		return tmp;
	}

	Matrix Matrix::operator-(const Matrix& matrix) {
		Matrix tmp(m_x, m_y);
		if ((m_x != matrix.m_x) || (m_y != matrix.m_y)) {
			for (int i = 0; i < m_x; i++)
				for (int j = 0; j < m_y; j++)
					tmp.m_matrix[i][j] = 0;
			std::cout << '\n' << "Operation cancelled!" << '\n';
			return tmp;
		}
		for (int i = 0; i < m_x; i++)
			for (int j = 0; j < m_y; j++)
				tmp.m_matrix[i][j] = m_matrix[i][j] + matrix.m_matrix[i][j];
		return tmp;
	}

	Matrix Matrix::operator*(const Matrix& matrix) {
		Matrix tmp(m_x, matrix.m_y);
		if (m_y != matrix.m_x) {
			for (int i = 0; i < m_x; i++)
				for (int j = 0; j < matrix.m_y; j++)
					tmp.m_matrix[i][j] = 0;
			std::cout << '\n' << "Operation cancelled!" << '\n';
			return tmp;
		}
		for (int i = 0; i < m_x; i++)
			for (int j = 0; j < matrix.m_y; j++) {
				int res = 0;
				for (int c = 0; c < m_y; c++) {
					res += m_matrix[i][c] * matrix.m_matrix[c][j];
				}
				tmp.m_matrix[i][j] = res;
			}
		return tmp;
	}

	int Matrix::determinant() {
		if (m_x == m_y) {
			if (m_x == 2)
				return (m_matrix[0][0] * m_matrix[1][1] - m_matrix[0][1] * m_matrix[1][0]);
			else if (m_x == 3)
				return (m_matrix[0][0] * m_matrix[1][1] * m_matrix[2][2]
					+ m_matrix[0][1] * m_matrix[1][2] * m_matrix[2][0]
					+ m_matrix[0][2] * m_matrix[1][0] * m_matrix[2][1]
					- m_matrix[0][2] * m_matrix[1][1] * m_matrix[2][0]
					- m_matrix[0][1] * m_matrix[1][0] * m_matrix[2][2]
					- m_matrix[0][0] * m_matrix[1][2] * m_matrix[2][1]);
			else {
				std::cout << '\n' << "Operation is not supported!" << '\n';
				return 0;
			}
		}
		else {
			std::cout << '\n' << "The matrix must be square!" << '\n';
			return 0;
		}
	}

	void Matrix::inverse(double** matrix_Inv) {
		if (m_x == m_y) {
			int det = this->determinant();
			if (det != 0) {
				if (m_x == 3) {
					double inv_det = 1.0 / det;
					matrix_Inv[0][0] = (m_matrix[1][1] * m_matrix[2][2] - m_matrix[2][1] * m_matrix[1][2]) * (inv_det / 1.0);
					matrix_Inv[1][0] = (m_matrix[0][1] * m_matrix[2][2] - m_matrix[0][2] * m_matrix[2][1]) * (inv_det / 1.0) * (-1.0);
					matrix_Inv[2][0] = (m_matrix[0][1] * m_matrix[1][2] - m_matrix[0][2] * m_matrix[1][1]) * (inv_det / 1.0);
					matrix_Inv[0][1] = (m_matrix[1][0] * m_matrix[2][2] - m_matrix[1][2] * m_matrix[2][0]) * (inv_det / 1.0) * (-1.0);
					matrix_Inv[1][1] = (m_matrix[0][0] * m_matrix[2][2] - m_matrix[0][2] * m_matrix[2][0]) * (inv_det / 1.0);
					matrix_Inv[2][1] = (m_matrix[0][0] * m_matrix[1][2] - m_matrix[1][0] * m_matrix[0][2]) * (inv_det / 1.0) * (-1.0);
					matrix_Inv[0][2] = (m_matrix[1][0] * m_matrix[2][1] - m_matrix[2][0] * m_matrix[1][1]) * (inv_det / 1.0);
					matrix_Inv[1][2] = (m_matrix[0][0] * m_matrix[2][1] - m_matrix[2][0] * m_matrix[0][1]) * (inv_det / 1.0) * (-1.0);
					matrix_Inv[2][2] = (m_matrix[0][0] * m_matrix[1][1] - m_matrix[1][0] * m_matrix[0][1]) * (inv_det / 1.0);
					for (int i = 0; i < 3; i++) {
						for (int j = 0; j < 3; j++)
							std::cout << matrix_Inv[i][j] << " ";
						std::cout << '\n';
					}
				}
				else if (m_x == 2) {
					double inv_det = 1.0 / det;
					matrix_Inv[0][0] = m_matrix[1][1] * (inv_det / 1.0);
					matrix_Inv[0][1] = m_matrix[0][1] * (inv_det / 1.0) * (-1.0);
					matrix_Inv[1][0] = m_matrix[1][0] * (inv_det / 1.0) * (-1.0);
					matrix_Inv[1][1] = m_matrix[0][0] * (inv_det / 1.0);
					for (int i = 0; i < 2; i++) {
						for (int j = 0; j < 2; j++)
							std::cout << matrix_Inv[i][j] << " ";
						std::cout << '\n';
					}
				}
				else {
					std::cout << '\n' << "Operation is not supported!" << '\n';
				}
			}
			else {
				std::cout << '\n' << "Determinant is 0!" << '\n';
			}
		}
		else {
			std::cout << '\n' << "The matrix must be square!" << '\n';
		}
	}

	Matrix Matrix::transpose() {
		Matrix temp(m_x, m_y);
		int t;
		for (int i = 0; i < m_x; i++)
			for (int j = i; j < m_y; j++) {
				temp.m_matrix[i][j] = m_matrix[j][i];
				temp.m_matrix[j][i] = m_matrix[i][j];
			}
		return temp;
	}

	Matrix::~Matrix() {
		for (int i = 0; i < m_x; i++)
			delete[] m_matrix[i];
		delete m_matrix;
	}
	
}