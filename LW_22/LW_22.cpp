﻿#include <iostream>
#include "matrix.h"

int main() {
	matrix::Matrix matrix_A(2, 2);

	matrix::Matrix matrix_B(3, 3);

	std::cin >> matrix_A;
	std::cin >> matrix_B;
	std::cout << "A:" << '\n' << matrix_A;
	std::cout << "B:" << '\n' << matrix_B;

	std::cout << '\n' << "Operation +:" << '\n' << matrix_A + matrix_B << '\n';
	std::cout << '\n' << "Operation *:" << '\n' << matrix_A * matrix_B << '\n';

	std::cout << "Determinant A: " << matrix_A.determinant() << '\n';
	std::cout << "Determinant B: " << matrix_B.determinant() << '\n';

	double** matrix_Inv = new double* [3];
	for (int i = 0; i < 3; i++)
		matrix_Inv[i] = new double[3];
	matrix_A.inverse(matrix_Inv);
	for (int i = 0; i < 3; i++)
		delete[] matrix_Inv[i];
	delete matrix_Inv;

	std::cout << "Transposed B: " << matrix_B.transpose() << '\n';

	return 0;
}