#pragma once
#include <iostream>

namespace matrix {
	class Matrix {
	private:
		int m_x, m_y;
		int** m_matrix;

	public:
		Matrix(int x, int y);

		Matrix(const Matrix& matrix);

		Matrix& operator=(const Matrix& matrix);

		Matrix operator+(const Matrix& matrix);

		Matrix operator-(const Matrix& matrix);

		Matrix operator*(const Matrix& matrix);

		int determinant();

		void inverse(double** matrix_Inv);

		Matrix transpose();

		~Matrix();

		friend std::istream& operator>>(std::istream& c_input, matrix::Matrix& matrix);
		friend std::ostream& operator<<(std::ostream& c_output, const matrix::Matrix& matrix);
	};

	
}