﻿#include <iostream>
#define N 5

float arif(float* arr, int c, float *res) {
    if (c > 0) {
        *res += arr[N - c];
        c--;
        arif(arr, c, res);
    }

    return (*res / N);
}

int main()
{
    float* arr = new float[N];
    int c = N;
    float res = 0;

    for (int i = 0; i < N; i++)
        std::cin >> arr[i];

    std::cout << "Result: " << arif(arr, c, &res) << '\n';

    delete[] arr;
    return 0;
}
