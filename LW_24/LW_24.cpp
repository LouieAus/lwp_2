﻿#include <iostream>
#include "bmp.h"

int main()
{
    bmp::FileBMP image_1("pointer.bmp");
    image_1.ImportImage();
    image_1.Brightness(-10);
    image_1.WriteImage("out.bmp");

    return 0;
}