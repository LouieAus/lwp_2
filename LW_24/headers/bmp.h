#pragma once
#include <string>
#include <fstream>
#include <cmath>

namespace bmp {

#pragma pack(1)
    struct BMPHEADER {
        unsigned short  type;           //Format signature
        unsigned int    file_size;      //Size of file
        unsigned short  reserved_1;     //Reserve 06/2
        unsigned short  reserved_2;     //Reserve 08/2
        unsigned int    off_bytes;      //Bytes to pixels array
    };
#pragma pack()

#pragma pack(1)
    struct DIBHEADER {
        unsigned int    size;           //Size of DIB-header structure
        int             width;          //Width of image
        int             height;         //Height of image
        unsigned short  planes;         //Variable for Windows icons and cursors
        unsigned short  bit_count;      //Number of bits per pixel
        unsigned int    compression;    //Variable for pixel storage method
        unsigned int    image_size;     //The size of pixel data in bytes
        int             x_per_meter;    //The number of pixels per meter horizontally
        int             y_per_meter;    //The number of pixels per meter vertically
        unsigned int    clr_used;       //Size of the color table
        unsigned int    clr_important;  //The number of cells from the beginning of the color table to the last used one
    };
#pragma pack()

    struct Pixel {
        unsigned char value;        //for CORE version (8 bit per pixel and lower)
        //unsigned char b, g, r;    //for 3, 4, 5 versions (8 bit per pixel and higher)
    };

	class FileBMP {
	private:
        std::string file_name;
        BMPHEADER bmpHeader;
        DIBHEADER dibHeader;
        unsigned char* color_table;
        Pixel** pixels;

    public:
        FileBMP(std::string name) {
            file_name = name;

            bmpHeader.type = 0;
            bmpHeader.file_size = 0;
            bmpHeader.reserved_1 = 0;
            bmpHeader.reserved_2 = 0;
            bmpHeader.off_bytes = 0;

            dibHeader.size = 0;
            dibHeader.width = 0;
            dibHeader.height = 0;
            dibHeader.planes = 0;
            dibHeader.bit_count = 0;
            dibHeader.compression = 0;
            dibHeader.image_size = 0;
            dibHeader.x_per_meter = 0;
            dibHeader.y_per_meter = 0;
            dibHeader.clr_used = 0;
            dibHeader.clr_important = 0;
        }

        /*  Input data from image   */
        void ImportImage() {
            std::ifstream input_file(file_name, std::ios::binary);

            //BMP-Header structure reading
            input_file.read(reinterpret_cast<char*>(&bmpHeader), sizeof(BMPHEADER));
            //DIB-Header structure reading
            input_file.read(reinterpret_cast<char*>(&dibHeader), sizeof(DIBHEADER));

            //Color table structure reading
            color_table = new unsigned char[bmpHeader.off_bytes - 54];
            if ((bmpHeader.off_bytes - 54) != 0)
                for (unsigned int i = 0; i < (bmpHeader.off_bytes - 54); i++)
                    input_file.read(reinterpret_cast<char*>(&color_table[i]), 1);

            //Image pixels structure reading
            pixels = new Pixel * [dibHeader.height];
            for (int i = 0; i < dibHeader.height; i++)
                pixels[i] = new Pixel[dibHeader.width];

            for (int i = 0; i < dibHeader.height; i++) {
                for (int j = 0; j < dibHeader.width; j++)
                    input_file.read(reinterpret_cast<char*>(&pixels[i][j]), sizeof(Pixel));

                if (dibHeader.width % 4 != 0)
                    for (int j = 0; j < (4 - dibHeader.width % 4); j++) {
                        char c;
                        input_file.read(&c, 1);
                    }
            }
        }

        /*  Output data to image   */
        void WriteImage(std::string output_file_name) {
            std::ofstream output_file(output_file_name, std::ios::binary);

            //BMP-Header structure of output image writing
            output_file.write(reinterpret_cast<char*>(&bmpHeader), sizeof(BMPHEADER));
            //DIB-Header structure of output image writing
            output_file.write(reinterpret_cast<char*>(&dibHeader), sizeof(DIBHEADER));

            //Color table structure of output image writing
            if ((bmpHeader.off_bytes - 54) != 0)
                for (unsigned int i = 0; i < (bmpHeader.off_bytes - 54); i++)
                    output_file.write(reinterpret_cast<char*>(&color_table[i]), 1);

            //Image pixels structure of output image writing
            for (int i = 0; i < dibHeader.height; i++) {
                for (int j = 0; j < dibHeader.width; j++) {
                    output_file.write(reinterpret_cast<char*>(&pixels[i][j]), sizeof(Pixel));
                }

                if (dibHeader.width % 4 != 0)
                    for (int j = 0; j < (4 - dibHeader.width % 4); j++) {
                        char c;
                        output_file.write(&c, 1);
                    }
            }
        }

        /*  Copy image header without file size and image size  */
        FileBMP ImageHeader() {
            FileBMP tmp("tmp.bmp");

            tmp.bmpHeader.type = bmpHeader.type;
            tmp.bmpHeader.file_size = 54;
            tmp.bmpHeader.off_bytes = 54;
            
            tmp.dibHeader.size = dibHeader.size;
            tmp.dibHeader.width = 0;
            tmp.dibHeader.height = 0;
            tmp.dibHeader.planes = dibHeader.planes;
            tmp.dibHeader.bit_count = dibHeader.bit_count;
            tmp.dibHeader.compression = dibHeader.compression;
            tmp.dibHeader.image_size = tmp.dibHeader.width * tmp.dibHeader.height;
            tmp.dibHeader.x_per_meter = dibHeader.x_per_meter;
            tmp.dibHeader.y_per_meter = dibHeader.y_per_meter;
            tmp.dibHeader.clr_used = dibHeader.clr_used;
            tmp.dibHeader.clr_important = dibHeader.clr_important;

            return tmp;
        }
        
        /*  Copy image without name   */
        FileBMP& operator=(const FileBMP& copy_file) {
            bmpHeader = copy_file.bmpHeader;
            dibHeader = copy_file.dibHeader;
            color_table = copy_file.color_table;

            pixels = new Pixel * [dibHeader.height];
            for (int i = 0; i < dibHeader.height; i++)
                pixels[i] = new Pixel[dibHeader.width];

            for(int i = 0; i < dibHeader.height; i++)
                for (int j = 0; j < dibHeader.width; j++) {
                    pixels[i][j] = copy_file.pixels[i][j];
                }
            return *this;
        }

        /*  Change image brightness  */
        void Brightness(int value) {
            for (int i = 0; i < bmpHeader.off_bytes - 54; i++)
                if ((i+1) % 4 != 0) {
                    if (value > 0) {
                        for (int c = 0; c < abs(value); c++)
                            if ((color_table[i] + 1) < 255)
                                color_table[i] += 1;
                    }
                    else {
                        for (int c = 0; c < abs(value); c++)
                            if ((color_table[i] - 1) > 0)
                                color_table[i] -= 1;
                    }
                }
        }

        ~FileBMP() {
            for (int i = 0; i < dibHeader.height; i++) {
                delete[] pixels[i];
            }
            delete[] pixels;
            delete color_table;
        }
	};

}