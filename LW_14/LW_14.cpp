﻿#include <iostream>
#include <ctime>
#define N 1000

void swap(int* a, int* b) {
	int c = *a;
	*a = *b;
	*b = c;
}

void bubbleSort(int* arr, int n) {
	for (int i = 1; i < n; i++) {
		if (arr[i] >= arr[i - 1])
			continue;
		int j = i - 1;
			while ((j >= 0) && (arr[j] > arr[j + 1]))
			{
				swap(&arr[j], &arr[j + 1]);
				j--;
			}
	}
}

int main() {
	int summ = 0;
	int* arr = new int[N];
	int num;
	for (int i = 0; i < N; i++) {
		arr[i] = rand();
		summ += arr[i];
		std::cout << i + 1 << ":   " << arr[i] << '\n';
	}
	if (summ >= 1000) {
		double start_time = clock();
		bubbleSort(arr, N);
		double end_time = clock();
		std::cout << '\n' << "Time: " << (end_time - start_time)/1000.0 << '\n';
		std::cout << "Sorted. " << '\n';
		//for (int i = 0; i < N - 1; i++)
			//std::cout << arr[i] << ", ";
		//std::cout << arr[N - 1] << ".";
	}
	else
		std::cout << "Not sorted." << '\n';
	delete[] arr;
	return 0;
}