﻿#include <iostream>
#include <cassert>
#include "matrix.h"

using mx::Mx2dbl;
using mx::Mx3dbl;

int main() {
	mx::Mx2dbl A({ {{1, 2},
					{3, 4}} });

	mx::Mx3dbl B({ {{1, 2, 3},
					{3, 4, 5},
					{8, 9, 7}} });

	std::cout << '\n' << '\n' << "-----------<<<<<<  Test 1: Operator (= and +)  >>>>>>-----------" << '\n' << '\n';
	auto C = A + A;
	assert(C.get(0, 0) == 2); assert(C.get(0, 1) == 4);
	assert(C.get(1, 0) == 6); assert(C.get(1, 1) == 8);

	std::cout << '\n' << '\n' << "-----------<<<<<<  Test 2: Operator (*)  >>>>>>-----------" << '\n' << '\n';
	C = A * A;
	assert(C.get(0, 0) == 7); assert(C.get(0, 1) == 10);
	assert(C.get(1, 0) == 15); assert(C.get(1, 1) == 22);

	std::cout << '\n' << '\n' << "-----------<<<<<<  Test 3: Determinant  >>>>>>-----------" << '\n' << '\n';
	assert(B.determinant() == 6);

	std::cout << '\n' << '\n' << "-----------<<<<<<  Test 4: Inverse  >>>>>>-----------" << '\n' << '\n';
	C = A.inverse();
	assert(C.get(0, 0) == -2); assert(C.get(0, 1) == 1);
	assert(C.get(1, 0) == 1.5); assert(C.get(1, 1) == -0.5);

	std::cout << '\n' << '\n' << "-----------<<<<<<  Test 5: Transpose  >>>>>>-----------" << '\n' << '\n';
	auto D = B.transpose();
	assert(D.get(0, 0) == 1); assert(D.get(0, 1) == 3); assert(D.get(0, 2) == 8);
	assert(D.get(1, 0) == 2); assert(D.get(1, 1) == 4); assert(D.get(1, 2) == 9);
	assert(D.get(2, 0) == 3); assert(D.get(2, 1) == 5); assert(D.get(2, 2) == 7);

	return 0;
}