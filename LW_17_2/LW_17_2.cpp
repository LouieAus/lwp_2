﻿#include <iostream>

double func(double x){
	return (x*x - 10);
}

double bisection(double a, double b) {
	int k = 1000000;
	double fa = func(a);
	double right = b;
	while (1) {
		double x = (a + b) / 2;
		if (round(x) == b) {
			a += b*0.9;
			b += b*0.9;
		}
		double rA = round(a * k) / k;
		double rB = round(b * k) / k;
		double rX = round(x * k) / k;
		if ((rX == rA) && (rX == rB)) {
			std::cout << round(rX) << '\n';
			return x;
		}
		else if ((func(x) * fa) > 0)
			a = x;
		else
			b = x;
	}
}

int main()
{
	std::cout << bisection(0, 1000000) << '\n';
	return 0;
}
