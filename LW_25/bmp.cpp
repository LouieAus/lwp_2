#pragma once
#include <string>
#include <fstream>
#include <cmath>
#include "matrix.h"
#include "bmp.h"
#define PI 3.14159265358979323846
#define RNUM 1000000000

//#define DEBUG

using mx::Vec2dbl;
using mx::Mx2dbl;

//Conversion from degrees to radians
double ToRadian(double degrees) {
    return ((degrees * PI) / 180);
}

//Find minimum or maximum among elements (first element, second element, find maximum?)
template<typename element_type>
element_type FindExtrem(element_type element_A, element_type element_B, bool maximum = false) {
    if (maximum)
        if (element_A <= element_B) return element_B; else return element_A;
    else if (element_A <= element_B) return element_A; else return element_B;
}

Oxy RotatePoint(Oxy point, float angle, Oxy rotation_point = { 0, 0 }) {
    Mx2dbl RotationMatrix({ { {round(cos(ToRadian(angle)) * RNUM) / RNUM, round(-1 * sin(ToRadian(angle)) * RNUM) / RNUM},
                              {round(sin(ToRadian(angle)) * RNUM) / RNUM, round(cos(ToRadian(angle)) * RNUM) / RNUM} } });

    Vec2dbl PointVec({ { {(point.x - rotation_point.x) + 0.0}, {(point.y - rotation_point.y) + 0.0} } });
    Vec2dbl ResultMatrix = RotationMatrix * PointVec;
    Oxy result_point = { ResultMatrix.get(0, 0) + rotation_point.x, ResultMatrix.get(1, 0) + rotation_point.y };

    return result_point;
}

namespace bmp {
    FileBMP::FileBMP(std::string name, int width, int height, bool header_init) {
        file_name = name;
        pivot = { 0, 0 };
        dibHeader.width = width;
        dibHeader.height = height;
        bmpHeader.reserved_1 = 0;
        bmpHeader.reserved_2 = 0;

        if (!header_init) { //Initialization without header data
            bmpHeader.type = 0;
            bmpHeader.file_size = 0;
            bmpHeader.off_bytes = 0;

            dibHeader.size = 0;
            dibHeader.planes = 0;
            dibHeader.bit_count = 0;
            dibHeader.compression = 0;
            dibHeader.image_size = 0;
            dibHeader.x_per_meter = 0;
            dibHeader.y_per_meter = 0;
            dibHeader.clr_used = 0;
            dibHeader.clr_important = 0;
        }
        else { //Initialization with header data
            bmpHeader.type = 0x424D;
            bmpHeader.off_bytes = 54;
            bmpHeader.file_size = bmpHeader.off_bytes + (width * height);

            dibHeader.size = 40;
            dibHeader.planes = 1;
            dibHeader.bit_count = 8;
            dibHeader.compression = 0;
            dibHeader.image_size = width * height;
            dibHeader.x_per_meter = 0;
            dibHeader.y_per_meter = 0;
            dibHeader.clr_used = 0;
            dibHeader.clr_important = 0;
        }
    }

    void FileBMP::ImportImage() {
        //this->Clear();

        std::ifstream input_file(file_name, std::ios::binary);

        //BMP-Header structure reading
        input_file.read(reinterpret_cast<char*>(&bmpHeader), sizeof(BMPHEADER));
        //DIB-Header structure reading
        input_file.read(reinterpret_cast<char*>(&dibHeader), sizeof(DIBHEADER));

        //Color table structure reading
        color_table = new unsigned char[bmpHeader.off_bytes - 54];
        if (bmpHeader.off_bytes != 0)
            for (unsigned int i = 0; i < (bmpHeader.off_bytes - 54); i++)
                input_file.read(reinterpret_cast<char*>(&color_table[i]), 1);

        //Image pixels structure reading
        pixels = new Pixel * [dibHeader.height];
        for (int i = 0; i < dibHeader.height; i++)
            pixels[i] = new Pixel[dibHeader.width];

        for (int i = 0; i < dibHeader.height; i++) {
            for (int j = 0; j < dibHeader.width; j++)
                input_file.read(reinterpret_cast<char*>(&pixels[i][j]), sizeof(Pixel));

            if (dibHeader.width % 4 != 0)
                for (int j = 0; j < (4 - dibHeader.width % 4); j++) {
                    char c;
                    input_file.read(&c, 1);
                }
        }
    }

    void FileBMP::WriteImage(std::string output_file_name) {
        std::ofstream output_file(output_file_name, std::ios::binary);

        //BMP-Header structure of output image writing
        output_file.write(reinterpret_cast<char*>(&bmpHeader), sizeof(BMPHEADER));
        //DIB-Header structure of output image writing
        output_file.write(reinterpret_cast<char*>(&dibHeader), sizeof(DIBHEADER));

        //Color table structure of output image writing
        if ((bmpHeader.off_bytes != 0) && (color_table != NULL)) {
            for (unsigned int i = 0; i < (bmpHeader.off_bytes - 54); i++)
                output_file.write(reinterpret_cast<char*>(&color_table[i]), 1);
        }

        //Image pixels structure of output image writing
        for (int i = 0; i < dibHeader.height; i++) {
            for (int j = 0; j < dibHeader.width; j++) {
                output_file.write(reinterpret_cast<char*>(&pixels[i][j]), sizeof(Pixel));
            }

            if (dibHeader.width % 4 != 0)
                for (int j = 0; j < (4 - dibHeader.width % 4); j++) {
                    char c;
                    output_file.write(&c, 1);
                }
        }
    }

    FileBMP FileBMP::ImageHeader() {
        FileBMP tmp("tmp.bmp");

        tmp.bmpHeader.type = bmpHeader.type;
        tmp.bmpHeader.file_size = 54;
        tmp.bmpHeader.off_bytes = 54;

        tmp.dibHeader.size = dibHeader.size;
        tmp.dibHeader.width = 0;
        tmp.dibHeader.height = 0;
        tmp.dibHeader.planes = dibHeader.planes;
        tmp.dibHeader.bit_count = dibHeader.bit_count;
        tmp.dibHeader.compression = dibHeader.compression;
        tmp.dibHeader.image_size = tmp.dibHeader.width * tmp.dibHeader.height;
        tmp.dibHeader.x_per_meter = dibHeader.x_per_meter;
        tmp.dibHeader.y_per_meter = dibHeader.y_per_meter;
        tmp.dibHeader.clr_used = dibHeader.clr_used;
        tmp.dibHeader.clr_important = dibHeader.clr_important;

        return tmp;
    }

    FileBMP& FileBMP::operator=(const FileBMP& copy_file) {
        bmpHeader = copy_file.bmpHeader;
        dibHeader = copy_file.dibHeader;
        color_table = copy_file.color_table;

        pixels = new Pixel * [dibHeader.height];
        for (int i = 0; i < dibHeader.height; i++)
            pixels[i] = new Pixel[dibHeader.width];

        for (int i = 0; i < dibHeader.height; i++)
            for (int j = 0; j < dibHeader.width; j++) {
                pixels[i][j] = copy_file.pixels[i][j];
            }
        return *this;
    }

    void FileBMP::Brightness(int value) {
        for (int i = 0; i < bmpHeader.off_bytes - 54; i++)
            if ((i + 1) % 4 != 0) {
                if (value > 0) {
                    for (int c = 0; c < abs(value); c++)
                        if ((color_table[i] + 1) < 255)
                            color_table[i] += 1;
                }
                else {
                    for (int c = 0; c < abs(value); c++)
                        if ((color_table[i] - 1) > 0)
                            color_table[i] -= 1;
                }
            }
    }

    void FileBMP::Clear(bool headers, bool colors, bool image) {
        if (image) {
            pixels = NULL;
            dibHeader.width = 0;
            dibHeader.height = 0;
        }
        if (colors) {
            if (bmpHeader.off_bytes != 0) {
                for (unsigned int i = 0; i < (bmpHeader.off_bytes - 54); i++)
                    color_table[i] = NULL;
            }
        }
        if (headers) {
            bmpHeader = { NULL, NULL, NULL, NULL, NULL };
            dibHeader = { NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL };
        }
    }

    void FileBMP::Fill(unsigned short int color_id, Oxy start_coord, Oxy end_coord, unsigned short int out_color) {
        if ((end_coord.x == 0) && (end_coord.y == 0)) {
            end_coord = { dibHeader.width - 1, dibHeader.height - 1 };
        }

        if (pixels == NULL) { //File hasn't pixel tabel
            dibHeader.width = end_coord.x;
            dibHeader.height = end_coord.y;

            pixels = new Pixel * [dibHeader.height];
            for (int i = 0; i < dibHeader.height; i++)
                pixels[i] = new Pixel[dibHeader.width];

            for (int i = 0; i < dibHeader.height; i++)
                for (int j = 0; j < dibHeader.width; j++) {
                    if (((start_coord.x <= j) && (j <= end_coord.x)) && ((start_coord.y <= i) && (i <= end_coord.y))) {
                        pixels[i][j].value = color_id;
                    }
                    else {
                        pixels[i][j].value = out_color;
                    }
                }
        }
        else { //File has pixel tabel
            for (int i = start_coord.x; i <= end_coord.x; i++)
                for (int j = start_coord.y; j <= end_coord.y; j++) {
                    pixels[i][j].value = color_id;
                }
        }
    }

    void FileBMP::ChangePivot(int x_coord, int y_coord) {
        pivot = { x_coord, y_coord };
    }

    void FileBMP::Rotate(int angle, unsigned short int color_id, unsigned short int interpolation, Oxy rotate_point) {
        if (rotate_point.x == -1)
            rotate_point = pivot;

        Oxy point_1 = RotatePoint({ 0, 0 }, angle, { rotate_point.x, rotate_point.y });                                         //Rotation of (0, 0)
        Oxy point_2 = RotatePoint({ dibHeader.width - 1, 0 }, angle, { rotate_point.x, rotate_point.y });                       //Rotation of (max X, 0)
        Oxy point_3 = RotatePoint({ 0, dibHeader.height - 1 }, angle, { rotate_point.x, rotate_point.y });                      //Rotation of (0, max Y)
        Oxy point_4 = RotatePoint({ dibHeader.width - 1, dibHeader.height - 1 }, angle, { rotate_point.x, rotate_point.y });    //Rotation of (max X, max Y)

        bool extrem = true; //max X finding
        int max_X = FindExtrem(point_1.x, FindExtrem(point_2.x, FindExtrem(point_3.x, point_4.x, extrem), extrem), extrem);
        extrem = false;     //min X finding
        int min_X = FindExtrem(point_1.x, FindExtrem(point_2.x, FindExtrem(point_3.x, point_4.x, extrem), extrem), extrem);

        extrem = true; //max Y finding
        int max_Y = FindExtrem(point_1.y, FindExtrem(point_2.y, FindExtrem(point_3.y, point_4.y, extrem), extrem), extrem);
        extrem = false;     //min Y finding
        int min_Y = FindExtrem(point_1.y, FindExtrem(point_2.y, FindExtrem(point_3.y, point_4.y, extrem), extrem), extrem);

        Oxy new_coord_axis = { min_X, min_Y };
        Oxy new_image_size = { max_X - min_X + 1, max_Y - min_Y + 1 };

        //Creating matrix of rotated points with chosen color
        Pixel** rotation_pixels = new Pixel * [new_image_size.y];
        for (int j = 0; j < new_image_size.y; j++) {
            rotation_pixels[j] = new Pixel[new_image_size.x];
            for (int i = 0; i < new_image_size.x; i++)
                rotation_pixels[j][i].value = color_id;
        }

        //Points of old matrix rotation and writing new point in matrix of rotated points
        for (int j = 0; j < dibHeader.height; j++)
            for (int i = 0; i < dibHeader.width; i++) {
                Oxy point_coord = RotatePoint({ i, j }, angle, { rotate_point.x, rotate_point.y });
                rotation_pixels[abs(point_coord.y - new_coord_axis.y)][abs(point_coord.x - new_coord_axis.x)] = pixels[j][i];
            }

        //Deleting of old matrix
        for (int i = 0; i < dibHeader.height; i++) {
            delete[] pixels[i];
        }
        delete[] pixels;

        //Changing width and height data of file
        dibHeader.width = new_image_size.x;
        dibHeader.height = new_image_size.y;
        bmpHeader.file_size = bmpHeader.off_bytes + (dibHeader.width * dibHeader.height);

        //Creating new pixel matrix
        pixels = new Pixel * [dibHeader.height];
        for (int i = 0; i < dibHeader.height; i++)
            pixels[i] = new Pixel[dibHeader.width];

        //Writing points in new pixel matrix and empty pixels fulling
        for (int j = 0; j < dibHeader.height; j++) {
            for (int i = 0; i < dibHeader.width; i++) {
                if (rotation_pixels[j][i].value == color_id) {
                    //Interpolation
                    if (interpolation == 0) {
                        int summ = 0;
                        int count = 0;

                        if ((j + 1) != dibHeader.height) { summ += rotation_pixels[j + 1][i].value;    count++; }
                        if ((j - 1) != -1) { summ += rotation_pixels[j - 1][i].value;    count++; }
                        if ((i + 1) != dibHeader.width) { summ += rotation_pixels[j][i + 1].value;    count++; }
                        if ((i - 1) != -1) { summ += rotation_pixels[j][i - 1].value;    count++; }

                        pixels[j][i].value = summ / count;
                    }
                    if (interpolation == 1) {
                        int pixels_arr[4];
                        int count = 0;

                        if ((j + 1) != dibHeader.height) { pixels_arr[count] = rotation_pixels[j + 1][i].value; count++; }
                        if ((j - 1) != -1) { pixels_arr[count] = rotation_pixels[j - 1][i].value; count++; }
                        if ((i + 1) != dibHeader.width) { pixels_arr[count] = rotation_pixels[j][i + 1].value; count++; }
                        if ((i - 1) != -1) { pixels_arr[count] = rotation_pixels[j][i - 1].value; count++; }
                        int max_count = 0;
                        int p_count;
                        int max_pixel_value = pixels_arr[0];
                        for (int t = 0; t < count; t++) {
                            p_count = 0;
                            for (int k = 1; k <= count - 1; k++) {
                                if (pixels_arr[0] == pixels_arr[k]) { p_count++; }
                            }
                            if (p_count > max_count) { max_count = p_count; max_pixel_value = pixels_arr[0]; }
                            if (t != count - 1) {
                                int tmp = pixels_arr[0];
                                pixels_arr[0] = pixels_arr[t + 1];
                                pixels_arr[t + 1] = tmp;
                            }
                        }
                        pixels[j][i].value = max_pixel_value;
                    }
                }
                else
                    pixels[j][i].value = rotation_pixels[j][i].value;
            }
        }

        //Deleting matrix of rotated points
        for (int i = 0; i < new_image_size.y; i++) {
            delete[] rotation_pixels[i];
        }
        delete[] rotation_pixels;
    }

    FileBMP::~FileBMP() {
        //std::cout << "Deleting " << file_name << '\n';
        for (int i = 0; i < dibHeader.height; i++) {
            delete[] pixels[i];
        }
        delete[] pixels;

        if (color_table != NULL)
            color_table = NULL;
    }
}
