﻿#include <iostream>
#include "bmp.h"

int main()
{
    bmp::FileBMP image_1("catm.bmp");
    image_1.ImportImage();

    image_1.Brightness(50);
    image_1.WriteImage("brightness.bmp");

    image_1.Rotate(30, 230);
    image_1.WriteImage("rotatation.bmp");

    image_1.Rotate(-150, 230);
    image_1.WriteImage("rotatation.bmp");

    return 0;
}