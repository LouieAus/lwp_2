#pragma once

struct Oxy {
    int x, y;
};

namespace bmp {
#pragma pack(1)
    struct BMPHEADER {
        unsigned short  type;           //Format signature
        unsigned int    file_size;      //Size of file
        unsigned short  reserved_1;     //Reserve 06/2
        unsigned short  reserved_2;     //Reserve 08/2
        unsigned int    off_bytes;      //Bytes to pixels array
    };
#pragma pack()

#pragma pack(1)
    struct DIBHEADER {
        unsigned int    size;           //Size of DIB-header structure
        int             width;          //Width of image
        int             height;         //Height of image
        unsigned short  planes;         //Variable for Windows icons and cursors
        unsigned short  bit_count;      //Number of bits per pixel
        unsigned int    compression;    //Variable for pixel storage method
        unsigned int    image_size;     //The size of pixel data in bytes
        int             x_per_meter;    //The number of pixels per meter horizontally
        int             y_per_meter;    //The number of pixels per meter vertically
        unsigned int    clr_used;       //Size of the color table
        unsigned int    clr_important;  //The number of cells from the beginning of the color table to the last used one
    };
#pragma pack()

    struct Pixel {
        unsigned char value;        //for CORE version (8 bit per pixel and lower)
    };

    class FileBMP {
    private:
        std::string file_name;          //Name of file
        BMPHEADER bmpHeader;            //BMP-header
        DIBHEADER dibHeader;            //DIB-header
        unsigned char* color_table;     //Colors tabel
        Pixel** pixels;                 //Pixels tabel
        Oxy pivot;
    public:
        //Constructor (file name, image width, image height, fill header?)
        FileBMP(std::string name, int width = 0, int height = 0, bool header_init = true);

        //Input data from image
        void ImportImage();

        //Output data to image (output-file name)
        void WriteImage(std::string output_file_name);

        //Copy image header without file size and image size
        FileBMP ImageHeader();

        //Copy image without name
        FileBMP& operator=(const FileBMP& copy_file);
        
        //Change image brightness (degree of brightness change)
        void Brightness(int value);

        //Clear header's, colors tabel's or pixels tabel's data (clear headers?, clear color table?, clear pixels table?)
        void Clear(bool headers = true, bool colors = true, bool image = true);

        //Fill image with or without pixel's tabel (filling color ID, initial coordinates, end coordinates, outside filling color ID)
        void Fill(unsigned short int color_id, Oxy start_coord = { 0, 0 }, Oxy end_coord = { 0, 0 }, unsigned short int out_color = 255);

        //Change pivot point (new pivot X coordinate, new pivot Y coordinate)
        void ChangePivot(int x_coord, int y_coord);

        //Rotate image (angle, out of the image filling, interpolation type, pivot coordinates)
        void Rotate(int angle, unsigned short int color_id, unsigned short int interpolation = 1, Oxy rotate_point = { -1, 0 });

        //Destructor
        ~FileBMP();
    };
}

