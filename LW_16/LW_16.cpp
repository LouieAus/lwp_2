﻿#include <iostream>
#include <ctime>
#define N 100

//BubbleSort
void bubbleSort(int n, int* arr) {
    for (int k = 0; k < n; k++) {
        for (int j = 0; j < n - k - 1; j++) {
            if (arr[j] > arr[j + 1]) {
                int buff = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = buff;
            }
        }
    }
}

//CountingSort
void countSort(int n, int *arr) {
    int max = arr[0];
    
    int* out = new int[n];

    for (int k = 1; k < n; k++)
        if (arr[k] > max) max = arr[k];

    int* count = new int[max + 1];
    for (int k = 0; k <= max; k++)
        count[k] = 0;

    for (int k = 0; k < n; k++)
        count[arr[k]]++;

    for (int k = 1; k <= max; k++)
        count[k] += count[k - 1];

    for (int k = 0; k < n; k++)
        out[count[arr[k]] - 1] = arr[k];

    for (int k = 0; k < n; k++)
        arr[k] = out[k];

    delete[] count;
    delete[] out;
}

//QuickSort
void swap(int* a, int* b) {
    int buff = *a;
    *a = *b;
    *b = buff;
}

int partition(int a, int b, int* arr) {
    int pivot = arr[b];
    int i = a - 1;
    for (int j = a; j < b; j++) {
        if (arr[j] <= pivot) {
            i++;
            swap(&arr[i], &arr[j]);
        }
    }
    swap(&arr[i + 1], &arr[b]);
    return (i + 1);
}

void quickSort(int a, int b, int* arr) {
    if (a < b) {
        int m = partition(a, b, arr);
        quickSort(a, m - 1, arr);
        quickSort(m + 1, b, arr);
    }
}


int main()
{
    int* arr = new int[N];
    for (int i = 0; i < N; i++)
        arr[i] = rand();

    double start_time = clock();
    quickSort(0, N - 1, arr);
    double end_time = clock();
    std::cout << '\n' << "Time: " << (end_time - start_time) / 1000.0 << '\n';

    for (int i = 0; i < N; i++)
        std::cout << "Element (index " << i << "): " << arr[i] << '\n';

    delete[] arr;
    return 0;
}
