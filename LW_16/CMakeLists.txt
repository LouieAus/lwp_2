cmake_minimum_required(VERSION 3.22 FATAL_ERROR)

project(LW_16)

set(SOURCES LW_16.cpp)

add_executable(LW_16 ${SOURCES})