#include <iostream>
#include "headers/code.h"

//#define DEBUG

namespace generator {

	//Class CodeGenerator
	CodeGenerator::CodeGenerator(const std::string input_filename, const std::string output_filename) {
		pseudocode_filename = input_filename;
		outputcode_filename = output_filename;
	}

	CodeGenerator::~CodeGenerator() {
#ifdef DEBUG
		std::cout << "CodeGenerator destructor" << '\n';
#endif
	}


	//Class JAVA
	JAVA::JAVA(const std::string input_filename, const std::string& output_name) : CodeGenerator::CodeGenerator(input_filename, output_name) {
#ifdef DEBUG
		std::cout << "JAVA constructor: " << output_name << '\n';
#endif
	}

	JAVA::~JAVA() {
#ifdef DEBUG
		std::cout << "JAVA destructor" << '\n';
#endif
	}

	std::string JAVA::Generate() {
		return this->outputcode_filename;
	}


	//Class C++
	C_PP::C_PP(const std::string input_filename, const std::string& output_name) : CodeGenerator::CodeGenerator(input_filename, output_name) {
#ifdef DEBUG
		std::cout << "C++ constructor: " << output_name << '\n';
#endif
	}

	C_PP::~C_PP() {
#ifdef DEBUG
		std::cout << "C++ destructor" << '\n';
#endif
	}

	std::string C_PP::Generate() {
		return this->outputcode_filename;
	}


	//Factory-function
	CodeGenerator* codeFactory(const std::string& file_name, const std::string& output_name) {
		int index = output_name.rfind('.');
		std::string code_format = output_name.substr(index, output_name.length() - index);

		CodeGenerator* language;

		if (code_format == ".java") language = new JAVA(file_name, output_name);
		else if (code_format == ".cpp") language = new C_PP(file_name, output_name);
		else throw "Bad language!";

		return language;
	}
}