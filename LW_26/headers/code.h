#pragma once
#include <fstream>

namespace generator {

	class CodeGenerator {
	protected:
		std::string pseudocode_filename;
		std::string outputcode_filename;
	public:
		CodeGenerator(const std::string input_filename, const std::string output_filename);
		virtual ~CodeGenerator();

		virtual std::string Generate() = 0;
	};

	class JAVA : public CodeGenerator {
	public:
		JAVA(const std::string input_filename, const std::string& output_name);
		virtual ~JAVA();

		std::string Generate() override;
	};

	class C_PP : public CodeGenerator {
	public:
		C_PP(const std::string input_filename, const std::string& output_name);
		virtual ~C_PP();

		std::string Generate() override;
	};

	//Factory-function
	CodeGenerator* codeFactory(const std::string& file_name, const std::string& output_name);
}