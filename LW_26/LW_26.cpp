﻿#include <iostream>
#include <vector>
#include "headers/code.h"

using namespace generator;

int main() {

	std::vector<CodeGenerator*> codes_vector;

	CodeGenerator* cpp_file = codeFactory("pseudocode_1.txt", "cpp_file.cpp");		codes_vector.push_back(cpp_file);
	CodeGenerator* java_file = codeFactory("pseudocode_2.txt", "java_file.java");	codes_vector.push_back(java_file);

	std::cout << cpp_file->Generate() << '\n';
	std::cout << java_file->Generate() << '\n';

	for (auto& file : codes_vector)
		delete[] file;

	return 0;
}