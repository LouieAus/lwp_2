#include "tree.h"

namespace tree {
	Elem* MAKE(int data, Elem* p) {
		Elem* q = new Elem;
		q->data = data;
		q->left = nullptr;
		q->right = nullptr;
		q->parent = p;
		q->high = NULL; //Глубина вершины
		return q;
	}

	void ADD(int data, Elem*& root) {
		//Создание корня, при его отсутствии
		if (root == nullptr) {
			root = MAKE(data, nullptr);
			root->high = 1;
			return;
		}
		Elem* v = root;
		//Нахождение родителя в дереве
		int c = 1;
		while ((data < (v->data) && (v->left) != nullptr) || (data > (v->data) && (v->right) != nullptr)) {
			c++;
			if (data < v->data) v = v->left;
			else v = v->right;
		}
		//Проверка на наличие в дереве такой же уже существующей вершины
		if (data == v->data) return;
		//Инициализация вершины
		Elem* u = MAKE(data, v);
		u->high = c + 1;
		//Указание в качестве потомка у родителя инициализированной вершины
		if (data < v->data) v->left = u;
		else v->right = u;
	}

	void PASS(Elem* v) {
		if (v == nullptr) return;
		std::cout << v->data << '\n';
		PASS(v->left);
		PASS(v->right);
	}

	void CHANGE_PASS(Elem* v, bool s) {
		if (v == nullptr) return;
		if(s) v->high--;
		else s = true;
		CHANGE_PASS(v->left, s);
		CHANGE_PASS(v->right, s);
	}

	Elem* SEARCH(int data, Elem* v) {
		//Возвращение элемента при его (не)нахождении
		if ((v == nullptr) || (v->data == data)) return v;
		//Поиск элемента
		if (data < v->data) return SEARCH(data, v->left);
		else return SEARCH(data, v->right);
	}

	void REMOVE(int data, Elem*& v, Elem*& root) {
		//Проверка на наличие элемента в дереве
		Elem* u = SEARCH(data, v);
		if (u == nullptr) return;
		//Удаление корня
		if (u->left == nullptr && u->right == nullptr && u == root) {
			root = nullptr;
			return;
		}
		//Поиск вершины для замены
		if ((u->left != nullptr && u->right != nullptr) || (((u->right != nullptr)) && (u->high == 1))) {
			Elem* t = u->right;
			while (t->left != nullptr) {
				t = t->left;
			}
			//Смещение глубины вершин на 1 вверх, находящихся под найденной вершиной
			CHANGE_PASS(t, false);

			u->data = t->data;
			u = t;
		}
		else if (((u->left != nullptr)) && (u->high == 1)) {
			Elem* t = u->left;
			while (t->right != nullptr) {
				t = t->right;
			}
			//Смещение глубины вершин на 1 вверх, находящихся под найденной вершиной
			CHANGE_PASS(t, false);

			u->data = t->data;
			u = t;
		}

		Elem* t;
		if (u->left == nullptr)
			t = u->right;
		else t = u->left;

		if (u->parent->left == u)
			u->parent->left = t;
		else u->parent->right = t;

		//Смещение глубины вершин на 1 вверх, находящихся под вершиной u
		CHANGE_PASS(u, false);

		if (t != nullptr) {
			t->parent = u->parent;
		}

		delete u;
	}

}
