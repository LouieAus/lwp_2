﻿#include <iostream>
#include <fstream>
#include <string>
#include <typeinfo>
#include "tree.h"

int main() {
	tree::Elem* root = nullptr;
	std::ifstream comFile("commands.txt");
	std::string line;
	std::string result = "";

	if (comFile.is_open()) {
		while (getline(comFile, line)) {
			char command = line[0]; //Первый символ (команда)
			if (command == 'E') break;

			std::string number;
			//Собираем символы после первого (число)
			for (std::string::size_type i = 1; i < line.length(); i++) {
				number = number + line[i];
			}

			if (command == '+') tree::ADD(std::stoi(number), root);
			else if (command == '?') {
				tree::Elem* el = tree::SEARCH(std::stoi(number), root);
				if (el != nullptr) result += std::to_string(el->high);
				else result += 'n';
			}
			else if (command == '-') {
				tree::REMOVE(std::stoi(number), root, root);
			}
		}
		comFile.close();
	}
	else std::cout << "File isn't opened!" << '\n';
	std::ofstream resFile;
	resFile.open("result.txt");
	//Запись в файл
	resFile << result;
	resFile.close();
	return 0;
}