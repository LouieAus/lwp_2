cmake_minimum_required(VERSION 3.22 FATAL_ERROR)

project(LW_20_5)

include_directories(headers)

set(SOURCES LW_20_5.cpp tree.cpp headers/tree.h)

add_executable(LW_20_5 ${SOURCES})