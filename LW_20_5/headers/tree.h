#pragma once
#include <iostream>

namespace tree {
	struct Elem {
		int data;
		Elem* left;
		Elem* right;
		Elem* parent;
		int high;
	};

	Elem* MAKE(int data, Elem* p); //Инициализации вершины

	void ADD(int data, Elem*& root); //Добавление вершины

	void PASS(Elem* v); //Проход по дереву

	void CHANGE_PASS(Elem* v, bool s); //Смещение глубины вершин вверх на 1 под вершиной v

	Elem* SEARCH(int data, Elem* v); //Нахождение элемента

	void REMOVE(int data, Elem*& v, Elem*& root); //Удаление элемента
}
