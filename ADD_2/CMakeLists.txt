cmake_minimum_required(VERSION 3.22 FATAL_ERROR)

project(ADD_2)

set(SOURCES ADD_2.cpp)

add_executable(ADD_2 ${SOURCES})