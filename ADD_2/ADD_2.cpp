﻿#include <iostream>
#include <vector>
#define N 50

void bubbleSort(int n, std::vector<int> &index, int *arr) {
    for (int k = 0; k < n; k++) {
        for (int j = 0; j < n - k - 1; j++) {
            if (arr[index[j]] > arr[index[j + 1]]) {
                int buff = arr[index[j]];
                arr[index[j]] = arr[index[j + 1]];
                arr[index[j + 1]] = buff;
            }
        }
    }
}

void schellSort(int n, int* arr, std::vector<int>&ciura) {
    int step = ciura[ciura.size() - 1];
    for (int j = 0; j < n; j++) {
        std::vector<int> index;
        for (int i = 0; i < (((n - j - 1) / step) + 1); i++) {
            index.push_back(j + step * i);
        }
        bubbleSort(index.size(), index, arr);
    }
    if (step != 1) {
        ciura.erase(ciura.begin() + ciura.size() - 1);
        schellSort(n, arr, ciura);
    }
}

int main()
{
    int* arr = new int[N];
    for (int i = 0; i < N; i++) {
        arr[i] = rand();
        std::cout << arr[i] << '\n';
    }
    std::cout << "------------" << '\n';

    std::vector<int> ciura = { 1, 4, 10, 23, 57, 132, 301, 701, 1750 };
    schellSort(N, arr, ciura);

    std::cout << "------------" << '\n';
    for (int i = 0; i < N; i++)
        std::cout << "Index " << i << ": " << arr[i] << '\n';

    delete[] arr;
    return 0;
}