#include <iostream>
#include "olist.h"

namespace olist {
	void ADD(O_List*& head, int data, int pos) {
		O_List* p = new O_List;
		p->data = data;

		int count = 0;
		O_List* k = head;
		while (count != pos-1) {
			if (k->next != nullptr) {
				count++;
				k = k->next;
			}
			else {
				std::cout << "Position was not found" << '\n';
				return;
			}
		}
		p->next = k->next;
		k->next = p;
	}

	void PASS(O_List* head) {
		O_List* p = head->next;
		while (p != nullptr) {
			std::cout << p->data << '\n';
			p = p->next;
		}
	}

	void CLEAR(O_List* head) {
		O_List* p = head->next;
		O_List* k;
		while (p != nullptr) {
			k = p;
			p = p->next;
			delete k;
		}
	}

	void REMOVE(O_List* head, int num) {
		O_List* p = head;
		O_List* k;
		while (1) {
			if (p->next == nullptr) {
				//std::cout << "Element was not found" << '\n';
				break;
			}
			if (p->next->data == num) {
				k = p->next;
				p->next = p->next->next;
				delete k;
				break;
			}
			else p = p->next;
		}
	}
}