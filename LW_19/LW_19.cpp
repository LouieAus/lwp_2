﻿#include <iostream>
#include "olist.h"
#define N 10000
#define D 1000

int main()
{
    olist::O_List* head = new olist::O_List;
    head->next = nullptr;

    for (int i = 1; i <= N; i++) olist::ADD(head, i, i);

    for (int i = 0; i < D; i++) olist::REMOVE(head, i);

    delete head;

    return 0;
}