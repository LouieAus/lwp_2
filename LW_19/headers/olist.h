#pragma once

namespace olist {
	struct O_List {
		O_List* next;
		int data;
	};

	void ADD(O_List*& head, int data, int pos); //���������� �������� �� �������

	void PASS(O_List* head); //������ �� ������

	void CLEAR(O_List* head); //������� ������

	void REMOVE(O_List* head, int num); //�������� ���������� �������� num
}