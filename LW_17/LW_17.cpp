﻿#include <iostream>
#include <ctime>
#define N 100000
#define M 10000

//QuickSort
void swap(int* a, int* b) {
    int buff = *a;
    *a = *b;
    *b = buff;
}

int partition(int a, int b, int* arr) {
    int pivot = arr[b];
    int i = a - 1;
    for (int j = a; j <= b - 1; j++) {
        if (arr[j] <= pivot) {
            i++;
            swap(&arr[i], &arr[j]);
        }
    }
    swap(&arr[i + 1], &arr[b]);
    return (i + 1);
}

void quickSort(int a, int b, int* arr) {
    if (a < b) {
        int m = partition(a, b, arr);
        quickSort(a, m - 1, arr);
        quickSort(m + 1, b, arr);
    }
}

//Binary Search
int binarySearch(int* arr, int n, int el) {
    if (el < arr[0]) return -1;
    if (el == arr[0]) return 0;
    if (el > arr[n - 1]) return -1;

    int a = 0;
    int b = n - 1;
    while ((a + 1) < b) {
        int c = (a + b) / 2;
        if (el > arr[c]) a = c;
        else b = c;
    }
    if (arr[b] == el) return b;
    else return -1;
}

int main()
{
    //Формирование массива
    int* arr = new int[N];
    int count = 0;
    for (int i = 0; i < N; i++)
        arr[i] = rand() % 1000 + 0;
    quickSort(0, N - 1, arr); //Сортировка массива
    /*for (int i = 0; i < N; i++) std::cout << "Index " << i << ": " << arr[i] << '\n';*/

    double all_time = 0;
    //Имитация поисковых запросов
    for (int k = 0; k < M; k++) {
        int num_find = rand() % 1000 + 0;
        int ind = -1;

        double start_time = clock();
        //ind = binarySearch(arr, N, num_find);
        for (int a = 0; a < N; a++) if (arr[a] == num_find) ind = a;
        double end_time = clock();
        all_time += (end_time - start_time) / 1000.0;

        /*if (ind != -1) std::cout << '\n' << "Found number " << num_find << " Index: " << ind << '\n';
        else std::cout << '\n' << "Number " << num_find << " isn't found!" << '\n';*/
    }
    std::cout << "Search algorithm time: " << all_time / M << " seconds." << '\n';
    std::cout << "All time of search: " << all_time << " seconds." << '\n';

    delete[] arr;
    return 0;
}
